extends Reference
class_name Constants
# This class contains constants such as global enums that should be accessible everywhere

const replace_color = Color.black
# Color in the various building icons that gets replaced with the player color

enum CARD_TYPE {
	BUILDING,
	ACTION,
}

enum ACTION_TYPE {
	MODIFY,
	ATTACK,
	TERRAIN,
}
