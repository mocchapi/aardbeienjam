extends Resource

var id:int
# Assigned on runtime

export var display_name:String
# Name shown in-game
export var minimum_science:int = 100
# Minimum science required to reach this era
export var subsequent_milestones:int = 10
# Amount of science after the minimum which unlocks a new card switch, chained
