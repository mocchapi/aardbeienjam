extends CardResource
class_name BuildingCard

export var max_hp:int = 1
# Base maximum hp this building has
export var max_fortification:int = 5
# Maximum foritification this building may have
# Foritification is a buffer that takes damage before health,
# but must be added through fortify action cards
export var science_production:int = 0
# Amount of housing this building provides
export var population_production:int = 0
# Amount of population this building provides
export var food_productin:int = 0
# Amount of food this building provides

export var attack_range:int = 0
# Range this building can attack other buildings in
# Setting this above 0 will mark this as an attack card
export var base_damage:int = 0
# Base amount of damage this building can deal
# (May be empowered by an action card)
export var extra_build_range:int = 0
# if positive, allows building `extra_build_range` tiles further from the nearest building

func _init():
	card_type = Constants.CARD_TYPE.BUILDING
