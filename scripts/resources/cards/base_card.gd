extends Resource
class_name CardResource

# A card that can get played
# Not intended to be used on its own, use buildng

var card_type:int
# see Constants.CARD_TYPE
var id:int
# Assigned at runtime
# Used for referencing cards over multiplayer cheaply

export var display_name:String
# Name displayed in game
export var energy_cost:int
# Energy cost to play this card
export(Resource) var era
# Era this card belongs to
# Era resource! not just any!
# Custom resources cannot be exported due to godot 3.5 shenanigans
export(Texture) var icon
# This gets shown on the card & on the board when played
# This is also the icon shown when a building is built
# Make sure to use the palette!
export(Script) var custom_behavior
# runs custom code when played, build, or more
