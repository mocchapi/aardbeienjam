extends ActionCard
class_name AttackActionCard

# Used on attack buildings to trigger an attack

export var damage_bonus:int = 0
# Positive or negative value which gets added to the base_damage on buildings
export var range_bonus:int = 0
# Positive or negative value which gets added to the base_attack_range on buildings

func _init():
	action_type = Constants.ACTION_TYPE.ATTACK
