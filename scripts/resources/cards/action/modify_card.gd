extends ActionCard
class_name ModifyActionCard

# Used on player buildings to give higher stats

export var fortify_bonus:int = 0
# Fortifies the building by X amount, up until its `max_fortification`
export var repair_bonus:int = 0
# Heals the building by X amount
# Fortifies the building by X amount, up until its `max_hp`

func _init():
	action_type = Constants.ACTION_TYPE.MODIFY
